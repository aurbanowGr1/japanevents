package JEvents;

import java.sql.Connection;
import java.sql.DriverManager;

import JEvents.domain.User;
import JEvents.entitybuilders.IEntityBuilder;
import JEvents.entitybuilders.UserEntityBuilder;
import JEvents.implementations.RepositoryUser;
import JEvents.repositories.IRepository;
import JEvents.unitofwork.IUnitOfWork;
import JEvents.unitofwork.UnitOfWork;

/**
        Klasa głowna uruchamiająca wszystkie pozostałe.
     */

public class App 
{
    public static void main( String[] args )
    {
        try
        {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            IEntityBuilder<User> builder = new UserEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<User> repo =
                    new RepositoryUser(connection,builder, uow);
            User p = new User();
            p.setLogin("jan");
            p.setPassword("Nowak");
            //repo.add(p);
            uow.commit();

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("koniec");
    }
}
