package JEvents.repositories;

/**
 * Interfejs korelujący z JEvents/domain/Category
 */

import JEvents.domain.Category;

public interface ICategoryRepository extends IRepository<Category> {
    public Category searchByCategoryName(String categoryName);
    //public Category searchByCategoryId(int categoryId);

}
