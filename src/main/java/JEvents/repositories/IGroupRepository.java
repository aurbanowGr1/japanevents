package JEvents.repositories;

/**
 * Interfejs korelujący z JEvents/domain/Group
 */

import JEvents.domain.Group;
import JEvents.domain.User;
import java.util.List;

public interface IGroupRepository extends IRepository<Group> {
    public Group getGroupByName(String groupName);
}