package JEvents.repositories;

/**
 * Interfejs korelujący z JEvents/domain/Event
 */

import JEvents.domain.Event;
import JEvents.domain.Category;
import JEvents.domain.User;

import java.util.List;

public interface IEventRepository extends IRepository<Event> {
    public Event getEventByTitle(String title);

    //public List<Event> getByCategoryName(String categoryName);
    //public List<Event> getByCategoryId(int categoryId);

    public List<Event> getByAuthorLogin(String login);
    public List<Event> getByAuthorId(int userId);
}
