package JEvents.repositories;

/**
 * Główny interfejs repozytorium
 */

import JEvents.domain.Entity;

import java.util.List;

public interface IRepository<TEntity extends Entity> {
    public TEntity get(int id);
    public List<TEntity> getAll();
    public void add(TEntity entity);
    public void delete(TEntity entity);
    public void update(TEntity entity);
}
