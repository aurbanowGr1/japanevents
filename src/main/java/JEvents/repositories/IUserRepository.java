package JEvents.repositories;

/**
 * Interfejs korelujący z JEvents/domain/User
 */

import JEvents.domain.User;
import JEvents.domain.Group;
import JEvents.implementations.RepositoryBase;

import java.util.*;

public interface IUserRepository {
    public User getUserByLogin(String login);
    //public User getUserByEmail(String email);

    //public List<User> getUserByGroupId(int groupId);
    public List<User> getUserByGroupName(String groupName);
    public List<User> showUserByLogin(String userLogin);
    public List<User> showUserById(int userId);
}
