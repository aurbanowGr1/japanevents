package JEvents.implementations;

import JEvents.domain.Group;
import JEvents.domain.User;
import JEvents.entitybuilders.IEntityBuilder;
import JEvents.repositories.IGroupRepository;
import JEvents.repositories.IUserRepository;
import JEvents.unitofwork.IUnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RepositoryGroup extends RepositoryBase<Group> {

    protected RepositoryGroup(Connection connection, IEntityBuilder<Group> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
    }

    @Override
    protected void prepareUpdateQuery(Group entity) throws SQLException {
        update.setString(1, entity.getGroup());
        update.setLong(2, entity.getId());
    }

    @Override
    protected void prepareAddQuery(Group entity) throws SQLException {
        save.setString(1, entity.getGroup());
    }

    @Override
    protected String getTableName() {
        return "Grupa";
    }

    @Override
    protected String getUpdateQuery() {
        return
                "UPDATE Grupa SET gname=? WHERE id=?";
    }

    @Override
    protected String getCreateQuery() {
        return
                "INSERT into Grupa (gname) VALUES (?)";
    }
}
