package JEvents.implementations;

import JEvents.domain.Group;
import JEvents.domain.User;
import JEvents.entitybuilders.IEntityBuilder;
import JEvents.repositories.IUserRepository;
import JEvents.unitofwork.IUnitOfWork;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class RepositoryUser extends RepositoryBase<User> {


    public RepositoryUser(Connection connection, IEntityBuilder<User> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
    }

    @Override
    protected void prepareUpdateQuery(User entity) throws SQLException {
        update.setString(1, entity.getLogin());
        update.setString(2, entity.getPassword());
        update.setLong(3, entity.getId());
    }

    @Override
    protected void prepareAddQuery(User entity) throws SQLException {
        save.setString(1, entity.getLogin());
        save.setString(2, entity.getPassword());
    }

    @Override
    protected String getTableName() {
        return "User";
    }

    @Override
    protected String getUpdateQuery() {
        return
                "UPDATE User SET login=?, password=? WHERE id=?";

    }

    @Override
    protected String getCreateQuery() {
        return
                "INSERT into User (login,password) VALUES (?,?)";
    }
}
