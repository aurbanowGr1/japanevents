package JEvents.implementations;

import JEvents.domain.Category;
import JEvents.domain.Event;
import JEvents.domain.User;
import JEvents.entitybuilders.IEntityBuilder;
import JEvents.repositories.ICategoryRepository;
import JEvents.unitofwork.IUnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RepositoryCategory extends RepositoryBase<Category> {

    protected RepositoryCategory(Connection connection, IEntityBuilder<Category> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
    }

    @Override
    protected void prepareUpdateQuery(Category entity) throws SQLException {
        update.setString(1, entity.getCategory());
        update.setLong(2, entity.getId());
    }

    @Override
    protected void prepareAddQuery(Category entity) throws SQLException {
        save.setString(1, entity.getCategory());
    }

    @Override
    protected String getTableName() {
        return "Category";
    }

    @Override
    protected String getUpdateQuery() {
        return
                "UPDATE Category SET name=? WHERE id=?";
    }

    @Override
    protected String getCreateQuery() {
        return
                "INSERT into Category (name) VALUES (?)";
    }
}
