package JEvents.domain;

/**
 * Created by Pariston on 2014-11-17.
 */
public enum EntityState {
    New, Changed, UnChanged, Deleted
}
