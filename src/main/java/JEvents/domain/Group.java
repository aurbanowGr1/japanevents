package JEvents.domain;

    /**
        Grupy określające niejako uprawnienia do zamieszczania wydarzeń (wpisów).
     */

import java.util.ArrayList;
import java.util. List;

public class Group extends Entity {
    private String group;
    private List<User> user;

    public Group() {
        user = new ArrayList<User>();
    }

    public List<User> getAllUsers() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
