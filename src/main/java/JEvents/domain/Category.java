package JEvents.domain;

import java.util.List;

/**
        Kategorie wpisów - wydarzeń (eventów) uwzględniane przez szukajkę.
     */

public class Category extends Entity {
    public String category;
    public Event event;
    public List<Event> events;

    public List<Event> getEvents() {
        return events;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}