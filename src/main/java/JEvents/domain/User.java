package JEvents.domain;

    /**
        Klasa dotycząca użytkowników - ustala ich login, hasło, sprawdza ich poprawność i tak dalej.
     */

import java.util.*;

public class User extends Entity {
    private String login;
    private String password;
    private Group group;
    public List<Event> events;

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}