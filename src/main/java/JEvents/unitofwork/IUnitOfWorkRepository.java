package JEvents.unitofwork;

import JEvents.domain.Entity;

/**
 * Created by Pariston on 2014-11-12.
 */
public interface IUnitOfWorkRepository {
    public void persistAdd(Entity entity);
    public void persistUpdate(Entity entity);
    public void persistDelete(Entity entity);
}
