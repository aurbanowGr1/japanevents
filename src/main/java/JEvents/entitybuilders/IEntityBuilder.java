package JEvents.entitybuilders;

import java.sql.ResultSet;

import JEvents.domain.Entity;

public interface IEntityBuilder<TEntity extends Entity> {

    public TEntity build(ResultSet row);
}