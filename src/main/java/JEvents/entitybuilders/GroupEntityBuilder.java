package JEvents.entitybuilders;

import JEvents.domain.Group;
import JEvents.domain.User;

import java.sql.ResultSet;
import java.util.List;

public class GroupEntityBuilder implements IEntityBuilder<Group> {
    @Override
    public Group build(ResultSet rs) {
        try
        {
            Group g = new Group();
            g.setGroup(rs.getString("gname"));
            return g;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
}
