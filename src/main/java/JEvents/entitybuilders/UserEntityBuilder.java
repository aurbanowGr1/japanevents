package JEvents.entitybuilders;

import JEvents.domain.User;

import java.sql.ResultSet;

public class UserEntityBuilder implements IEntityBuilder<User> {
    @Override
    public User build(ResultSet rs) {
        try
        {
            User u = new User();
            //u.setId(rs.getInt("id"));
            u.setLogin(rs.getString("login"));
            u.setPassword(rs.getString("password"));
            return u;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
}
