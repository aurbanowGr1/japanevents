package JEvents.entitybuilders;

import JEvents.domain.Event;

import java.sql.ResultSet;

public class EventEntityBuilder implements IEntityBuilder<Event> {
    @Override
    public Event build(ResultSet rs) {
        try
        {
            Event e = new Event();
            e.setTitle(rs.getString("title"));
            e.setDescription(rs.getString("description"));
            e.setDate(rs.getString("category"));
            e.setHour(rs.getString("address"));
            e.setAddress(rs.getString("data"));
            e.setAddress(rs.getString("hour"));
            return e;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
}
