package JEvents.implementations;

import JEvents.domain.Category;
import JEvents.entitybuilders.CategoryEntityBuilder;
import JEvents.entitybuilders.IEntityBuilder;
import JEvents.repositories.IRepository;
import JEvents.unitofwork.IUnitOfWork;
import JEvents.unitofwork.UnitOfWork;
import junit.framework.TestCase;

import java.sql.Connection;
import java.sql.DriverManager;

public class RepositoryCategoryTest extends TestCase {

    public void testPrepareUpdateQuery() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            IEntityBuilder<Category> builder = new CategoryEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<Category> repo =
                    new RepositoryCategory(connection,builder, uow);
            Category p = new Category();
            p.setId(1);
            p.setCategory("xcategory");                      //id w bazie = 1
            repo.add(p);
            assertNotNull(p);
            Category d = new Category();
            d.setId(2);
            d.setCategory("othercategory");                   //id w bazie = 2
            repo.add(d);
            p.setCategory("editedxcategory");
            repo.update(p);
            uow.commit();
            assertNotSame(repo.get(1), repo.get(2));
            System.out.println(repo.get(1));
            System.out.println(repo.get(2));
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testPrepareAddQuery() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            IEntityBuilder<Category> builder = new CategoryEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<Category> repo =
                    new RepositoryCategory(connection,builder, uow);
            Category p = new Category();
            p.setCategory("kategoria1");
            repo.add(p);
            uow.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}