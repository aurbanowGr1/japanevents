package JEvents.implementations;

import JEvents.domain.Event;
import JEvents.entitybuilders.EventEntityBuilder;
import JEvents.entitybuilders.IEntityBuilder;
import JEvents.repositories.IRepository;
import JEvents.unitofwork.IUnitOfWork;
import JEvents.unitofwork.UnitOfWork;
import junit.framework.TestCase;

import java.sql.Connection;
import java.sql.DriverManager;

public class RepositoryEventTest extends TestCase {

    public void testPrepareUpdateQuery() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            IEntityBuilder<Event> builder = new EventEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<Event> repo =
                    new RepositoryEvent(connection,builder, uow);
            Event p = new Event();
            p.setId(1);
            p.setHour("17:30");
            p.setTitle("xevent");                      //id w bazie = 1
            repo.add(p);
            assertNotNull(p);
            Event d = new Event();
            d.setId(2);
            d.setHour("17:00");
            d.setTitle("evenrdaniel");                   //id w bazie = 2
            repo.add(d);
            p.setTitle("eventekdanielasa");
            repo.update(p);
            uow.commit();
            assertNotSame(repo.get(1), repo.get(3));
            //System.out.println(repo.get(1));
            //System.out.println(repo.get(2));
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testPrepareAddQuery() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            IEntityBuilder<Event> builder = new EventEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<Event> repo =
                    new RepositoryEvent(connection,builder, uow);
            Event p = new Event();
            p.setTitle("event");
            p.setDescription("opis pierwszego");
            p.setAddress("Warszawa ul. Jakas");
            p.setDate("2015-04-15");
            p.setHour("19:30");
            repo.add(p);
            uow.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}